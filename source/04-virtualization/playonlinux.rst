
###############
**PlayOnLinux**
###############



Install `PlayOnLinux <https://www.playonlinux.com/>`_:

.. code-block:: shell
   :linenos:

   sudo apt install -y playonlinux winbind

You may hide the virtual drives directory:

.. code-block:: shell
   :linenos:

   echo "PlayOnLinux's virtual drives" >> $HOME/.hidden



.. TEMP

    Microsoft Office 2010
    =====================

    Installez Office 2010 avec PlayOnLinux.

    Installez et utilisez la dernière versions stable de Wine dans :menuselection:`Configurer --> Général`.

    Installez le Service Pack 2 dans :menuselection:`Configurer --> Divers --> Lancer un .exe dans ce disque virtuel`.

    .. note::

        Si les accents circonflexes ne fonctionnent pas aller dans :menuselection:`Paramètres --> Prise en charge des langues`
        et modifier ``Système de saisie au clavier`` en ``Aucun``.



    Microsoft Office 2016
    =====================

    TODO



    Ant Renamer
    ===========

    Téléchargez puis installez `Ant Renamer <https://antp.be/software/renamer/download/fr>`_.
