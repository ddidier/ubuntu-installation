
#########################
**Ansible** IT automation
#########################



Install `Ansible <https://www.ansible.com/>`_ for all users:

.. code-block:: shell
   :linenos:

   sudo apt install -y ansible

To install multiple versions of Ansible for one user, use :doc:`VirtualEnvs <python>`.
