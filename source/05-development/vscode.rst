
#############################
**Visual Studio Code** editor
#############################



Install `Visual Studio Code <https://code.visualstudio.com/>`_:

- using Snap:

    .. code-block:: shell
       :linenos:

       sudo snap install code --classic

- using the offcial PPA:

    .. code-block:: shell
       :linenos:

       curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /tmp/packages.microsoft.gpg
       sudo install -o root -g root -m 644 /tmp/packages.microsoft.gpg /usr/share/keyrings/
       echo "deb [arch=amd64 signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" | sudo tee /etc/apt/sources.list.d/vscode.list

       sudo apt update
       sudo apt install -y code

Some useful plugins:

- `Markdown All in One <https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one>`__
- `Markdown Lint <https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint>`__
- `Markdown Table <https://marketplace.visualstudio.com/items?itemName=TakumiI.markdowntable>`__
