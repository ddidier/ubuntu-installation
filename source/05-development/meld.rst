
#######################
**Meld** diff and merge
#######################



Install `Meld <https://meldmerge.org/>`_:

.. code-block:: shell
   :linenos:

   sudo apt install -y meld



Configuration
=============

Customize the settings using the preferences dialog:

Customize the settings using the Meld settings dialog :menuselection:`Meld --> Preferences` or the command line:

- :menuselection:`Editor --> Tab width` = ``4``

.. code-block:: shell

   gsettings set org.gnome.meld indent-width 4

- :menuselection:`Editor --> Insert spaces instead of tabs` = ``On``

.. code-block:: shell

   gsettings set org.gnome.meld insert-spaces-instead-of-tabs true

- :menuselection:`Editor --> Highlight current line` = ``On``

.. code-block:: shell

   gsettings set org.gnome.meld highlight-current-line true

- :menuselection:`Editor --> Show line number` = ``On``

.. code-block:: shell

   gsettings set org.gnome.meld show-line-numbers true

- :menuselection:`Editor --> Use syntax highlighting` = ``On``

.. code-block:: shell

   gsettings set org.gnome.meld highlight-syntax true



Files browser integration
=========================

See :doc:`/02-base-utilities/nautilus` and :doc:`/02-base-utilities/nemo`.
