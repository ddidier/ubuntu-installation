
#################
**Ruby** language
#################



RVM environnements manager
==========================

Install `RVM <https://rvm.io/>`_ as a normal user and not as ``root``:

.. code-block:: shell
   :linenos:

   sudo apt install -y gnupg2
   curl -sSL https://rvm.io/mpapis.asc | gpg2 --import -
   curl -sSL https://get.rvm.io | bash -s stable --ruby



RubyMine editor
===============

Install `RubyMine <https://www.jetbrains.com/ruby/>`_:

.. code-block:: shell
   :linenos:

   sudo snap install rubymine --classic
