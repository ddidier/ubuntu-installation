
#######################
**Git** version control
#######################



Git command line client
=======================

Install Git:

.. code-block:: shell
   :linenos:

   sudo apt install -y git git-lfs



SmartGit graphical client
=========================

Download then install `SmartGit <https://www.syntevo.com/smartgit/>`_:

.. code-block:: shell
   :linenos:

   SMARTGIT_VERSION=X_Y_Z
   wget https://www.syntevo.com/downloads/smartgit/smartgit-${SMARTGIT_VERSION}.deb -O /tmp/smartgit-${SMARTGIT_VERSION}.deb
   sudo gdebi -n /tmp/smartgit-${SMARTGIT_VERSION}.deb
