
#######################
**Firefox** web browser
#######################



Firefox is installed by default.



Settings
========

.. todo:: Configure



Addons
======

Install some extensions:

- `Auto Tab Discard <https://addons.mozilla.org/fr/firefox/addon/auto-tab-discard/>`_
- `Awesome Screenshot <https://addons.mozilla.org/firefox/addon/screenshot-capture-annotate/>`_
- `Bitwarden <https://addons.mozilla.org/firefox/addon/bitwarden-password-manager/>`_
- `DownThemAll <https://addons.mozilla.org/fr/firefox/addon/downthemall/>`_
- `Evernote Web Clipper <https://addons.mozilla.org/firefox/addon/evernote-web-clipper/>`_
- `Facebook Container <https://addons.mozilla.org/firefox/addon/facebook-container/>`_
- `Firefox Multi Account Containers <https://addons.mozilla.org/firefox/addon/multi-account-containers/>`_
- `GNOME Shell integration <https://addons.mozilla.org/firefox/addon/gnome-shell-integration/>`_
- `Grammalecte <https://addons.mozilla.org/firefox/addon/grammalecte-fr/>`_
- `GreaseMonkey <https://addons.mozilla.org/firefox/addon/greasemonkey/>`_
- `HTTPS Everywhere <https://addons.mozilla.org/firefox/addon/https-everywhere/>`_
- `JsonView <https://addons.mozilla.org/firefox/addon/jsonview/>`_
- `Most Recent Tab <https://addons.mozilla.org/firefox/addon/most-recent-tab/>`_
- `NordVPN <https://addons.mozilla.org/firefox/addon/nordvpn-proxy-extension/>`_
- `NoScript <https://addons.mozilla.org/firefox/addon/noscript/>`_
- `Session Sync <https://addons.mozilla.org/firefox/addon/session-sync/>`_
- `Stylus <https://addons.mozilla.org/fr/firefox/addon/styl-us/>`_
- `Synology Download Manager <https://addons.mozilla.org/firefox/addon/synology-download-manager/>`_
- `Tree Style Tab <https://addons.mozilla.org/firefox/addon/tree-style-tab/>`_
- `uBlock Origin <https://addons.mozilla.org/firefox/addon/ublock-origin/>`_
- `Video DownloadHelper <https://addons.mozilla.org/firefox/addon/video-downloadhelper/>`_
- `Violent Monkey <https://addons.mozilla.org/fr/firefox/addon/violentmonkey/>`_


.. figure:: _images/firefox-extensions-icons.png
   :align: center
   :scale: 75%

   Extensions icons order


Firefox Multi-Account Containers
--------------------------------

In the preferences:

- Enable Sync = ``On``


Most Recent Tab
---------------

In the preferences:

- Keyboard shortcut = ``Ctrl + 1``


NoScript
--------

In the preferences:

- Import configuration


Tree Style Tab
--------------

In the startup page:

- "Bookmark This Tree" in the context menu on tabs, ... = ``On``
- Don't expand collapsed tree and skip collapsed descendants, ... = ``On``

In :menuselection:`Preferences --> Tree Behavior`:

- :menuselection:`When a new tree appears, collapse others automatically` = ``Off``
- :menuselection:`When a tab gets focus, expand its tree and collapse others automatically` = ``Off``
- :menuselection:`Don't expand collapsed tree and skip collapsed descendants, ...` = ``Off``
- :menuselection:`Double-click on a tab"` = ``Collapse/expand tree``
- :menuselection:`When a parent tab is closed or moved just as a solo tab` = ``Replace closed parent with a dummy tab and keep the tree``

Enable custom stylesheets in ``about:config``:

- ``toolkit.legacyUserProfileCustomizations.stylesheets`` = ``true``

Then create ``~/.mozilla/firefox/<profile>/chrome/userChrome.css``:

.. code-block:: css
   :linenos:

    @namespace url("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul");

    /* Remove horizontal tabs (with Tree Style Tab) */
    #tabbrowser-tabs {
      visibility: collapse;
    }

    /* Remove sidebar header (with Tree Style Tab) */
    #sidebar-header {
      visibility: collapse;
    }


Video DownloadHelper
--------------------

Install `Video DownloadHelper Companion <https://www.downloadhelper.net/install-coapp?browser=firefox>`_.

In the preferences:

- :menuselection:`Behavior --> Default download directory` = ``/home/ddidier/Downloads``



Synchronize
===========

Synchronize the account...
