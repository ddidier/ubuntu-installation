
##########################
**Nautilus** files browser
##########################



Nautilus is installed by default.



Configuration
=============

- :menuselection:`Toggle View` = ``List``

  .. code-block:: shell

     gsettings set org.gnome.nautilus.preferences default-folder-viewer 'list-view'

Customize the settings using the preferences dialog or the command line:

- :menuselection:`Views --> List View --> Allow folders to be expanded` = ``True``

  .. code-block:: shell

     gsettings set org.gnome.nautilus.list-view use-tree-view true

- :menuselection:`Behavior --> Link Creation --> Show action to create symbolic links` = ``True``

  .. code-block:: shell

     gsettings set org.gnome.nautilus.preferences show-create-link true

- :menuselection:`Search & Preview --> Search` = ``All locations``

  .. code-block:: shell

     gsettings set org.gnome.nautilus.preferences recursive-search 'always'

- :menuselection:`Search & Preview --> Thumbnails` = ``All files``

  .. code-block:: shell

     gsettings set org.gnome.nautilus.preferences show-image-thumbnails 'always'



Extensions
==========

Install some extensions:

.. code-block:: shell
   :linenos:

   sudo apt install -y nautilus-image-converter

   # nautilus-compare was removed in Ubuntu 20.04
   sudo add-apt-repository -y ppa:boamaod/nautilus-compare
   sudo apt install -y nautilus-compare



To open a new tab in an already running instance, instead of a new instance:

.. code-block:: diff
   :caption: /usr/share/nautilus-compare/nautilus-compare.py
   :linenos:

   @@ -48,7 +48,7 @@
               self.for_later = paths[0]
               return

   +       args = "-n "
   -       args = ""
           for path in paths:
               args += "\"%s\" " % path
