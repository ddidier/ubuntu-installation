
##########################
**Command line** utilities
##########################



``atop`` is an interactive monitor to view the load on a Linux system.
It shows the occupation of the most critical hardware resources (from a performance point of view) on system level, i.e. cpu, memory, disk and network.

.. code-block:: shell
   :linenos:

   sudo apt install -y atop



``autojump`` is a faster way to navigate your filesystem.
It works by maintaining a database of the directories you use the most from the command line.
`More details... <https://github.com/wting/autojump>`__.

.. code-block:: shell
   :linenos:

   sudo apt install -y autojump



``btop`` is a resource monitor that shows usage and stats for processor, memory, disks, network and processes.
`More details... <https://github.com/aristocratos/btop>`__.

.. code-block:: shell
   :linenos:

   sudo snap install btop
   sudo snap connect btop:removable-media



``fd`` is a program to find entries in your filesystem. It is a simple, fast and user-friendly alternative to find.
While it does not aim to support all of find's powerful functionality, it provides sensible (opinionated) defaults for a majority of use cases.
`More details... <https://github.com/sharkdp/fd>`__.

.. code-block:: shell
   :linenos:

   sudo apt install -y fd-find



``gdebi`` is an APT tool which can be used in command-line and on the GUI.
It can install a local ``.deb`` file via the command line like the ``dpkg`` command, but with access to repositories to resolve dependencies.

.. code-block:: shell
   :linenos:

   sudo apt install -y gdebi-core



``htop`` is an interactive system-monitor process-viewer and process-manager.
It is designed as an alternative to the Unix program top.

.. code-block:: shell
   :linenos:

   sudo apt install -y htop



``tree`` is a recursive directory listing program that produces a depth indented listing of files.

.. code-block:: shell
   :linenos:

   sudo apt install -y tree



``unrar`` is an unarchiver for ``.rar`` files (non-free version).

.. code-block:: shell
   :linenos:

   sudo apt install -y unrar
