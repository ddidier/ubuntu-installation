
##########################
**BackInTime** backup tool
##########################



Remove Déjà Dup then install BackInTime:

.. code-block:: shell
   :linenos:

   sudo apt purge -y deja-dup

   sudo add-apt-repository ppa:bit-team/stable
   sudo apt install -y backintime-qt4

Do not forget to create a backup plan!

You may also create a backup plan for ``/etc`` with the ``root`` user.



.. alternatives:

   - https://borgbackup.readthedocs.io/en/stable/ with https://github.com/borgbase/vorta
