
############################
**Sublime Text** text editor
############################



Install `Sublime Text <https://www.sublimetext.com/>`_:

.. code-block:: shell
   :linenos:

   wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg
   echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
   sudo apt update
   sudo apt install -y sublime-text



Packages
========

Install *Package Control* in the menu :menuselection:`Tools --> Install Package Control...`

Install the following packages:

- `A File Icon <https://packagecontrol.io/packages/A%20File%20Icon>`_ -- Sublime Text File-Specific Icons for Improved Visual Grepping
- `Ansible <https://packagecontrol.io/packages/Ansible>`_ -- Syntax highlighting for Ansible
- `Apply Syntax <https://packagecontrol.io/packages/ApplySyntax>`_ -- Syntax detector for Sublime Text
- `Dockerfile Syntax Highlighting <https://packagecontrol.io/packages/Dockerfile%20Syntax%20Highlighting>`_ -- Dockerfile syntax
- `Git Config <https://packagecontrol.io/packages/Git%20Config>`_ -- Sublime Text 2 language file for .gitconfig and .gitignore files
- `Indent XML <https://packagecontrol.io/packages/Indent%20XML>`_ -- Plugin for Sublime Text editor for reindenting XML and JSON files
- `Jinja2 <https://packagecontrol.io/packages/Jinja2>`_ -- Jinja2 Syntax Support for TextMate and Sublime Text
- `JSON Key Value <https://packagecontrol.io/packages/JSON%20Key-Value>`_ -- Alternative language definition for the JSON syntax
- `Language French Français <https://packagecontrol.io/packages/Language%20-%20French%20-%20Fran%C3%A7ais>`_ -- French Spelling Language for Sublime Text
- `LiveReload <https://packagecontrol.io/packages/LiveReload>`_ -- LiveReload plugin for SublimeText
- `Markdown Editing <https://packagecontrol.io/packages/MarkdownEditing>`_ -- Powerful Markdown package for Sublime Text
- `Markdown Preview <https://packagecontrol.io/packages/Markdown%20Preview>`_ -- Markdown preview and build plugin for Sublime Text
- `Markdown TOC <https://packagecontrol.io/packages/MarkdownTOC>`_ -- Generate a table of contents (TOC) in a markdown document
- `ReStructured Text Snippets <https://packagecontrol.io/packages/Restructured%20Text%20(RST)%20Snippets>`_ --  Restructured Text snippets and code completion hotkeys
- `Sass <https://packagecontrol.io/packages/Sass>`_ -- Sass and SCSS syntax for Sublime Text
- `Search Anywhere <https://packagecontrol.io/packages/Search%20Anywhere>`_ -- Quickly search on multiple search engines from the current selection
- `SideBarEnhancements <https://packagecontrol.io/packages/SideBarEnhancements>`_ -- Enhancements to Sublime Text sidebar. Files and folders.



Integration
===========

To associate text files with Sublime Text:

- directly use the files browser, or
- edit ``~/.config/mimeapps.list``, e.g. ``text/plain=sublime_text.desktop`` (cf. :ref:`/base-utilities/gnome#files-associations`)
