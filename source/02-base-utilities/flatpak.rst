
###########################
**Flatpak** package manager
###########################



Install Flatpak:

.. code-block:: shell
   :linenos:

   sudo apt install -y flatpak
   flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
