
######################
**Terminator** console
######################



Install `Terminator <https://terminator-gtk3.readthedocs.io/>`_:

.. code-block:: shell
   :linenos:

   sudo apt install -y terminator



Customize the settings using the preferences dialog:

- :menuselection:`Profiles --> General --> Copy on selection` = ``True``

- :menuselection:`Profiles --> Scrolling --> Infinite scrollback` = ``True``

- :menuselection:`Profiles --> Scrolling --> Scroll on output` = ``False``

- :menuselection:`Plugins`: disable all plugins



Install the `Search on the Internet plugin <https://github.com/choffee/terminator-plugins>`_:

.. code-block:: shell
   :linenos:

   mkdir -p ~/.config/terminator/plugins
   wget -P ~/.config/terminator/plugins https://raw.github.com/choffee/terminator-plugins/master/searchplugin.py

Then enable it in the preferences.


Set Terminator as default terminal:

.. code-block:: shell
   :linenos:

   gsettings set org.gnome.desktop.default-applications.terminal exec 'terminator'

There is no way to replace the default terminal used by Nautilus or Nemo.

Gnome Terminal must be replaced:

.. code-block:: shell
   :linenos:

   sudo mv /usr/bin/gnome-terminal /usr/bin/gnome-terminal.bak
   sudo ln -s /usr/bin/terminator /usr/bin/gnome-terminal
