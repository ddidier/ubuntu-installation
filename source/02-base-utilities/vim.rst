
###################
**Vim** text editor
###################



Install Vim:

.. code-block:: shell
   :linenos:

   sudo apt install -y vim

Customize the settings using `The Ultimate Vim configuration <https://github.com/amix/vimrc>`_:

.. code-block:: shell
   :linenos:

   sudo apt install -y git

   git clone --depth=1 https://github.com/amix/vimrc.git $HOME/.vim_runtime
   $HOME/.vim_runtime/install_basic_vimrc.sh
