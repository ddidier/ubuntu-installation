
########################
**PDFsam** PDF utilities
########################



Download then install `PDFsam <http://www.pdfsam.org/>`__:

.. code-block:: shell
   :linenos:

   PDFSAM_VERSION_PATH=v4.3.0
   PDFSAM_VERSION=4.3.0-1
   wget https://github.com/torakiki/pdfsam/releases/download/${PDFSAM_VERSION_PATH}/pdfsam_${PDFSAM_VERSION}_amd64.deb -O /tmp/pdfsam_${PDFSAM_VERSION}_amd64.deb
   sudo gdebi -n /tmp/pdfsam_${PDFSAM_VERSION}_amd64.deb
