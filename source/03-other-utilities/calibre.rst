
##########################
**Calibre** e-book manager
##########################



Install or update `Calibre <https://calibre-ebook.com/>`_:

.. code-block:: shell
   :linenos:

   sudo -v \
   && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh \
   | sudo sh /dev/stdin
