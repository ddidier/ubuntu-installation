
############################
**FontBase** font management
############################



Download then install `FontBase <https://fontba.se/>`_:

.. code-block:: shell
   :linenos:

   FONTBASE_VERSION=2.17.5
   mkdir -p ~/Applications/FontBase
   wget https://releases.fontba.se/linux/FontBase-${FONTBASE_VERSION}.AppImage -O ~/Applications/FontBase/FontBase-${FONTBASE_VERSION}.AppImage
   ln -s FontBase-${FONTBASE_VERSION}.AppImage ~/Applications/FontBase/FontBase.AppImage
   chmod a+x ~/Applications/FontBase/FontBase-${FONTBASE_VERSION}.AppImage
