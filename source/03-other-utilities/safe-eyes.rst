
########################
**Safe Eyes** protection
########################



Install `Safe Eyes <https://slgobinath.github.io/SafeEyes/>`_:

.. code-block:: shell
   :linenos:

   sudo add-apt-repository -y ppa:slgobinath/safeeyes
   sudo apt install -y safeeyes

Configure Safe Eyes:

- :menuselection:`Settings --> Long breaks --> Interval between two breaks` = `80`
- :menuselection:`Settings --> Long breaks --> Break duration` = ``600``
- :menuselection:`Settings --> Options --> Allow postponing breaks` = ``On``
- :menuselection:`Settings --> Options --> Postpone duration` = ``1``
- :menuselection:`Plugins --> Audible Alert` = ``Off``
- :menuselection:`Plugins --> Health Statistics` = ``On``
