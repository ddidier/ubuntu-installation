
##############################
**InSync** Google Drive client
##############################



Install `InSync <https://www.insynchq.com/>`_:

.. code-block:: shell
   :linenos:

   sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys ACCAF35C
   echo "deb http://apt.insync.io/ubuntu $(lsb_release -cs) non-free contrib" | sudo tee /etc/apt/sources.list.d/insync.list
   sudo apt update
   sudo apt install -y insync
