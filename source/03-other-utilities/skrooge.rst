
#############################
**Skrooge** personal finances
#############################



Install `Skrooge <https://skrooge.org/>`_:

.. code-block:: shell
   :linenos:

   sudo apt install -y skrooge

   # or from the official PPA
   sudo add-apt-repository -y ppa:s-mankowski/ppa-kf5
   sudo apt install -y skrooge-kf5
