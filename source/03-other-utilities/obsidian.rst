
###########################
**Obsidian** knowledge base
###########################



Download then install `Obsidian <https://obsidian.md/>`__:

.. code-block:: shell
   :linenos:

   OBSIDIAN_VERSION=0.14.15
   wget https://github.com/obsidianmd/obsidian-releases/releases/download/v${OBSIDIAN_VERSION}/obsidian_${OBSIDIAN_VERSION}_amd64.deb -O /tmp/obsidian_${OBSIDIAN_VERSION}_amd64.deb
   sudo gdebi -n /tmp/obsidian_${OBSIDIAN_VERSION}_amd64.deb
