
######
Energy
######



.. error:: This part requires a complete overhaul.

   Some links of interest:

   - `Mate Optimus <https://github.com/ubuntu-mate/mate-optimus>`_
   - `SlimBook Battery <https://slimbook.es/en/tutoriales/aplicaciones-slimbook/398-slimbook-battery-3-application-for-optimize-battery-of-your-laptop>`_
   - `Hibernate Status Button <https://extensions.gnome.org/extension/755/hibernate-status-button/>`_
   - https://github.com/d4nj1/TLPUI



Power management
================

TLP brings you the benefits of advanced power management for Linux without the need to understand every technical detail.
TLP comes with a default configuration already optimized for battery life, so you may just install and forget it.

.. code-block:: shell
   :linenos:

   sudo apt install -y tlp tlp-rdw
   sudo tlp start



Enable hibernation
==================

.. warning:: The size of the Swap partition must be larger than the total amount of RAM if you want the system to be able to hibernate.

To test hibernation, run:

.. code-block:: shell
   :linenos:

   sudo systemctl hibernate

To enable hibernation in the UI:

1. install the Gnome extension `Hibernate Status Button <https://extensions.gnome.org/extension/755/hibernate-status-button/>`__

2. create the file with the following content:

.. code-block:: shell
   :caption: /etc/polkit-1/localauthority/50-local.d/com.ubuntu.desktop.pkla
   :linenos:

   [Enable hibernate in upower]
   Identity=unix-user:*
   Action=org.freedesktop.upower.hibernate
   ResultActive=yes

   [Enable hibernate in logind]
   Identity=unix-user:*
   Action=org.freedesktop.login1.hibernate;org.freedesktop.login1.handle-hibernate-key;org.freedesktop.login1;org.freedesktop.login1.hibernate-multiple-sessions;org.freedesktop.login1.hibernate-ignore-inhibit
   ResultActive=yes
