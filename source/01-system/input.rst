
#####
Input
#####



Logitech Unifying
=================

*Solaar* is a Linux device manager for Logitech's Unifying Receiver peripherals.

.. code-block:: shell
   :linenos:

   sudo apt install -y solaar



Logitech MX Revolution
======================

To search Google for the selected text when the :guilabel:`Zoom` button is pressed.

*Easystroke Gesture Recognition* is a gesture-recognition application.
*xclip* is a command line interface to X selections (clipboard).

.. code-block:: shell
   :linenos:

   sudo apt install -y easystroke xclip

Open *Easystroke Gesture Recognition* then:

1. in :menuselection:`Preferences --> Appearance`

   a. check :guilabel:`Autostart easystroke`

2. in :menuselection:`Preferences --> Behavior --> Additional Buttons`

   a. press :guilabel:`Add`
   b. select :guilabel:`Instant Gestures`
   c. press the :guilabel:`Zoom` button in the gray area

3. in :menuselection:`Actions` press :guilabel:`Add Action` then

   a. doucle-click the column *Stroke* then press the :guilabel:`Zoom` button
   b. set *Name* to ``Search Google``
   c. set *Type* to ``Command``
   d. set *Details* to ``sh -c 'firefox "https://www.google.com/search?q=$(xclip -o)"'``
