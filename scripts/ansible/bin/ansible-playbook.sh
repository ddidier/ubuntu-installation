#! /bin/bash

set -e

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
PROJECT_DIR="$(realpath "$SCRIPT_DIR/..")"

BOLD_RED='\e[1;31m'
BOLD_GREEN='\e[1;32m'
BOLD_YELLOW='\e[1;33m'

TXT_RESET='\e[0m'

function help() {
  local error_message="$1"
  local error_code="$2"

  if [ -n "$error_message" ]; then
    echo -e "${BOLD_RED}$error_message${TXT_RESET}"
  fi

  echo -e "Usage: ./ansible-playbook.sh INVENTORY <ANSIBLE_OPTIONS...>"
  echo -e "  INVENTORY: the name of the inventory directory"
  echo -e "  ANSIBLE_OPTIONS: the standard Ansible options (except the inventory)"
  echo -e "Example: ./ansible-playbook.sh testing playbooks/deploy.yml"

  if [ -n "$error_code" ]; then
    exit "$error_code"
  fi
}

function main() {
  #  local command_arguments=()
  #  local positional_arguments=()
  #  local argument_type="command"
  #
  #  # shellcheck disable=SC2034
  #  for i in "$@"; do
  #      case "$1" in
  #          --)
  #              argument_type="positional"
  #              shift
  #              ;;
  #          *)
  #              if [[ "$argument_type" == "command" ]]; then
  #                  command_arguments+=("$1")
  #              elif [[ "$argument_type" == "positional" ]]; then
  #                  positional_arguments+=("$1")
  #              fi
  #              shift
  #              ;;
  #      esac
  #  done
  #
  #  my_command \
  #      "${command_arguments[@]}" \
  #      -- \
  #      -e root_dir="$PROJECT_DIR" \
  #      "${positional_arguments[@]}"

  if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
    help
    exit 0
  fi

  local ansible_inventory="$1"
  local ansible_arguments=("${@:2}")

  # ----- check parameters

  if [ -z "$ansible_inventory" ]; then
    help "Inventory must be specified" 1
  fi

  local inventory_directory="${PROJECT_DIR}/inventories/${ansible_inventory}"

  if [ ! -d "$inventory_directory" ]; then
    help "The inventory directory must exist: ${inventory_directory}" 1
  fi

  local inventory_file="${inventory_directory}/hosts.ini"

  if [ ! -f "$inventory_file" ]; then
    help "The inventory file must exist: ${inventory_file}" 1
  fi

  local configuration_file="${PROJECT_DIR}/ansible.cfg"
  local libraries_directory="${PROJECT_DIR}/libraries"
  local root_directory="${PROJECT_DIR}"

  # ----- find global variables files

  readarray -t extra_variables_arguments \
    < <(find "$inventory_directory" \
      -mindepth 1 -maxdepth 1 \
      -regextype posix-extended -regex "^.+\\.(yml|yaml)$" \
      -printf '--extra-vars\n@%p\n')

  # ----- run Ansible Playbook

  echo -e "${BOLD_YELLOW}"
  echo -e "Running ansible-playbook with:"
  echo -e "- Root directory .......... $root_directory"
  echo -e "- Ansible configuration ... $configuration_file"
  echo -e "- Ansible inventory ....... $inventory_file"
  echo -e "- Ansible libraries ....... $libraries_directory"
  echo -e "- Ansible arguments ....... ${extra_variables_arguments[*]} ${ansible_arguments[*]}"
  echo -e "${TXT_RESET}"

  ANSIBLE_CONFIG="$configuration_file" \
    ansible-playbook \
    --inventory "$inventory_file" \
    --extra-vars root_dir="$root_directory" \
    --module-path "$libraries_directory" \
    "${extra_variables_arguments[@]}" \
    "${ansible_arguments[@]}"
}

main "$@"
