# ndd-ubuntu

<!-- MarkdownTOC -->

1. [Requirements](#requirements)
1. [Variables](#variables)
    1. [Naming variables](#naming-variables)
    1. [Global variables](#global-variables)
    1. [Common variables](#common-variables)
    1. [Encrypted variables](#encrypted-variables)
1. [Dependencies](#dependencies)
1. [Run playbook](#run-playbook)
1. [Development](#development)
    1. [Best practices](#best-practices)
    1. [Test](#test)
        1. [Preparing tests](#preparing-tests)
        1. [Writing tests](#writing-tests)
        1. [Running tests](#running-tests)
1. [License](#license)
1. [Versions](#versions)
1. [References](#references)

<!-- /MarkdownTOC -->



<div class="placeholder">
    <p>
A brief description of the Playbook goes here.
    </p>
</div>

"TODO: Add a meaningful description".



<a id="requirements"></a>
## Requirements

<div class="placeholder">
    <p>
Any pre-requisites that may not be covered by Ansible itself or the playbook should be mentioned here. For instance, if the playbook uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.
    </p>
</div>



<a id="variables"></a>
## Variables

<a id="naming-variables"></a>
### Naming variables

Since there is no namespace in Ansible, naming variables is important to avoid collision:

- variables are written in snake case.
- variables are prefixed with the playbook name acting as a namespace, ending with 2 underscores.

The pattern is then `<playbook_namespace>__<variable_name>`,
for example `ndd__environment` (notice the 2 underscores).

<a id="global-variables"></a>
### Global variables

Variables defined in the `group_vars` and `host_vars` are applied to hosts - they are NOT available globally.
To make variables globally available - and not be tied to a specific host, you need to pass them as *extra variables*.
Either pass them as `--extra-vars "my_global_var=toto"` or via a file `--extra-vars @vars.dev`.
We prefer the file approach for reason of clarity.
These kind of variables are defined in the files `inventories/<inventory_name>/vars.yml`.

This playbook requires the `root_dir` variable to be set which is the `ndd-ubuntu` directory.
This is where the `inventories`, `playbooks` and `roles` directories are located.
This can be done for example by using `--extra-vars root_dir=$ROOT_DIR` with the `ansible-playbook` command.

<a id="common-variables"></a>
### Common variables

Variables common to all the inventories are located in files inside the `inventories/commons` subdirectories.
These files are the targets of links inside other inventories, e.g.

```
inventories/production/group_vars/all/_commons.yml ⟶ inventories/commons/group_vars/all/vars.yml
inventories/staging/group_vars/all/_commons.yml    ⟶ inventories/commons/group_vars/all/vars.yml
inventories/testing/group_vars/all/_commons.yml    ⟶ inventories/commons/group_vars/all/vars.yml
```

The links inside the actual inventory must come before any other file in order to be parsed first.
Any variable in these files has then a chance to be overridden if needed.

Use the script `inventories/link-commons.sh` to create the links when required.

<a id="encrypted-variables"></a>
### Encrypted variables

- Encryption must be done at the file level, not the variable level.
- Encrypted variables must have a layer of indirection.
- Encrypted variable names must start with ``vault__``.

For example, create two files:

- `inventories/<inventory_name>/group_vars/<group_name>/vars.yml`
- `inventories/<inventory_name>/group_vars/<group_name>/vars.vault.yml`

The first one holds a clear variable referencing the encrypted one, e.g. `clear_text: {{vault__clear_text}}`.

The second one holds the variable to be encrypted, e.g. `vault__clear_text: some value`, and is encrypted:

```bash
ansible-vault create inventories/<inventory_name>/group_vars/<group_name>/vars.vault.yml
```

See the official documentations for more details:

- [Ansible Vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html)
- [Ansible Vault best practices](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html#variables-and-vaults)



<a id="dependencies"></a>
## Dependencies

<div class="placeholder">
    <p>
A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.
    </p>
</div>

Dependencies cannot be committed as part of this playbook.
Dependencies are listed in the file `roles/requirements.yml` following the [Ansible Galaxy syntax].
Dependencies are installed with `roles/requirements.sh`.



<a id="run-playbook"></a>
## Run playbook

Use the script `bin/ansible-playbook.sh`:

```bash
% ./bin/ansible-playbook -h
Usage: ./ansible-playbook.sh INVENTORY <ANSIBLE_OPTIONS...>
  INVENTORY: the name of the inventory directory
  ANSIBLE_OPTIONS: the standard Ansible options (except the inventory)
Example: ./ansible-playbook.sh testing playbooks/deploy.yml
```

This script take care of setting:

- the Ansible configuration file
- the Ansible inventory file according to the given argument
- the Ansible libraries directory
- the Ansible configuration file
- the Ansible extra variables files



<a id="development"></a>
## Development

<a id="best-practices"></a>
### Best practices

You should follow some best practices, for example:

- [Ansible Best Practices](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html)
- [WhiteCloud Ansible Styleguide](https://github.com/whitecloud/ansible-styleguide)
- [Wiredcraft Ansible Template](https://github.com/Wiredcraft/ansible-template)
- [Some Ansible Best Practices](https://github.com/GSA/datagov-deploy/wiki/Best-Practices#ansible)
- [More Ansible Best Practices](https://github.com/enginyoyen/ansible-best-practises)

<div class="placeholder">
    <p>
A list of your own rules to follow and best practices.
    </p>
</div>

<a id="test"></a>
### Test

<a id="preparing-tests"></a>
#### Preparing tests

The testing framework is [Molecule].

The Python package manager is [PIP].

We strongly suggest that you use [pyenv].

Testing requires [Docker] and/or [Vagrant] with [VirtualBox] 6.0.

On Ubuntu 18.04:

<div class="placeholder">
    <p>
Change the GIT URL below.
    </p>
</div>

```bash
# install some useful dependencies
sudo apt install -y build-essential libssl-dev libffi-dev python-dev

# install PIP and VirtualEnv
sudo apt install -y python3-pip python3-venv

# install pyenv, and
# do not forget to read the installation output!
curl https://pyenv.run | bash

# clone the template repository
git clone https://your.git.server/ndd-ubuntu.git
cd ndd-ubuntu

# create and activate the environments as given in the '.python-version' file
pyenv install 3.8.2
pyenv virtualenv 3.8.2 ndd-ubuntu_3.8.2
pip install --upgrade pip

# install all the required dependencies
pip install --requirement molecule/requirements.txt
```

<a id="writing-tests"></a>
#### Writing tests

Tests can be written in two different ways :

- using [pytest] framework
- using the [testinfra] framework (based on *pytest*)

The framework *testinfra* tests the features from the node being tested by Molecule.
The assertions are not very helpful IMHO, e.g. a service named `httpd` is running and listening on the port `80`.

The framework *pytest* tests the features from the host running Molecule.
You can use any library that you want to test a real business value, e.g. a web service is answering on the port `80`.

<a id="running-tests"></a>
#### Running tests

Test the playbook with the `molecule test` command that will run all the following stages:

```
└── default
    ├── lint
    ├── destroy
    ├── dependency
    ├── syntax
    ├── create
    ├── prepare
    ├── converge
    ├── idempotence
    ├── side_effect
    ├── verify
    └── destroy
```

If you want to develop and test incrementally, you may use these commands:

- `molecule test --destroy never`: run the tests and keep the containers or the VM.
- `molecule verify`: run the tests only.

Molecule will silence log output, unless invoked with the `--debug` flag, e.g.

- `molecule --debug test --destroy never`
- `molecule --debug verify`

A file containing the Ansible vault password must be present to decrypt the encrypted files.
Export the standard environment variable `ANSIBLE_VAULT_PASSWORD_FILE` before running molecule, e.g.:

```bash
ANSIBLE_VAULT_PASSWORD_FILE=/path-to-my-playbook/molecule/default/vault-password.txt molecule test
```



<a id="license"></a>
## License

<div class="placeholder">
    <p>
        Do not forget to set a license on your work!
        The (same) license must be set in the following files:
    </p>
    <ul>
        <li>README.md (here)</li>
        <li>LICENSE.txt</li>
        <li>meta/main.yml</li>
    </ul>
</div>



<a id="versions"></a>
## Versions

This project has been generated using [NDD ansible-playbook-template] in the following state:

- generation date: `2020/05/14 21:41:44`
- commit hash: `46ad5e28e9462899c2a401573b958d8490f5ffa2`
- commit date: `2020/05/14 21:36:07`
- commit tags: ``
- modified: `False`

These data may be used to incorporate later changes of the `ndd-playbook-template` project into this project.

- generation date: when the current project has been generated based on the `ndd-playbook-template` project.
- commit hash: the head commit of the `ndd-playbook-template` project at generation time.
- commit date: the head commit of the `ndd-playbook-template` project at generation time.
- commit tags: the head commit of the `ndd-playbook-template` project at generation time.
- modified: True when there is uncommitted changes in the `ndd-playbook-template` project at generation time, False otherwise.



<a id="references"></a>
## References

[Ansible]: https://www.ansible.com/
[Ansible Galaxy syntax]: https://docs.ansible.com/ansible/latest/reference_appendices/galaxy.html#installing-multiple-roles-from-a-file
[Ansible variable precedence]: https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable
[CookieCutter]: https://github.com/audreyr/cookiecutter
[Docker]: https://www.docker.com/
[HTTPd]: https://httpd.apache.org/
[Molecule]: https://molecule.readthedocs.io/
[NDD ansible-playbook-template]: https://gitlab.com/ddidier/ansible-playbook-template
[PIP]: https://en.wikipedia.org/wiki/Pip_(package_manager)
[pyenv]: https://github.com/pyenv/pyenv
[pytest]: https://docs.pytest.org/
[testinfra]: https://testinfra.readthedocs.io/
[Vagrant]: https://www.vagrantup.com/
[VirtualBox]: https://www.virtualbox.org/
[VirtualEnv]: https://virtualenv.pypa.io/
[VirtualEnvWrapper]: https://virtualenvwrapper.readthedocs.io/

- [Ansible]
- [Ansible Galaxy syntax]
- [Ansible variable precedence]
- [CookieCutter]
- [Docker]
- [HTTPd]
- [Molecule]
- [PIP]
- [pyenv]
- [pytest]
- [testinfra]
- [Vagrant]
- [VirtualBox]
- [VirtualEnv]
- [VirtualEnvWrapper]



<style type="text/css">
    .placeholder::before {
        content: "TODO: ";
    }
    .placeholder {
        background-color: hsl(39,100%,75%);
        border: 1px solid hsl(39,100%,50%);
        font-style: italic;
        padding: 5px 10px;
    }
    .placeholder pre {
        background-color: hsl(39,100%,80%);
    }
    p + .placeholder {
        margin-top: 1rem;
    }
    .placeholder + p {
        margin-top: 1rem;
    }
</style>
