import os
import requests
import testinfra.utils.ansible_runner

# Access to Ansible
ansible_runner = testinfra.utils.ansible_runner.AnsibleRunner(os.environ['MOLECULE_INVENTORY_FILE'])
# Facts by host
hosts_facts = {}
# IP addresses by host
hosts_ips = {}

# -----
# Get the hosts whose features are tested.
tested_hosts = ansible_runner.get_hosts('tested')

# WARNING: Facts gathering is slow! Activate only if really needed!
for tested_host in tested_hosts:
    host_facts = ansible_runner.run_module(tested_host, 'setup', None)['ansible_facts']
    hosts_facts[tested_host] = host_facts

# -----
# Get the hosts from which the features are tested.
# Use this if you really need to have a container acting as a client,
# but it's way better to directly test from your host...
#
# testing_hosts = ansible_runner.get_hosts('testing')
#
# WARNING: Facts gathering is slow! Activate only if really needed!
# for testing_host in testing_hosts:
#     host_facts = ansible_runner.run(testing_host, 'setup')['ansible_facts']
#     hosts_facts[testing_host] = host_facts

# -----
for host, facts in hosts_facts.items():
    host_ip = facts['ansible_eth1']['ipv4']['address']
    hosts_ips[host] = host_ip

# -----
# The tests are run against all hosts unless the test contains a 'testinfra_hosts' list.
# This list can contain hostnames or ansible groups.
testinfra_hosts = tested_hosts

# IMPORTANT: This Python script is executed on the host running Molecule.


# --------------------------------------------------------------- Internal -----
# Execute parts of the tests on the node being tested by Molecule.
# Very slow...

def test_httpd_service_is_running_and_enabled(host):
    # remotely executed on the tested node:
    service = host.service('httpd')
    assert service.is_running
    assert service.is_enabled


def test_httpd_service_is_listening(host):
    socket = host.socket("tcp://0.0.0.0:80")
    assert socket.is_listening


# --------------------------------------------------------------- External -----
# Execute the tests on the host running Molecule.
# Very fast!

def test_httpd_service_is_responding():
    for tested_host in tested_hosts:
        tested_host_url = f'http://{hosts_ips[tested_host]}/'
        response = requests.get(tested_host_url)
        # the status code is 403...
        assert response.status_code == 403
        assert "Apache HTTP server" in response.text
