#! /usr/bin/env python3

import os
import shutil


VALID_DRIVERS = {
    'd': 'docker',
    'v': 'vagrant',
}


def ask_for_driver():
    response = None

    while response not in VALID_DRIVERS.keys():
        print()
        print("The available Molecule drivers are:")
        for driver_name in VALID_DRIVERS.values():
            first_letter = driver_name[0].upper()
            remaining_letters = driver_name[1:]
            print(f'  - [{first_letter}]{remaining_letters}')

        response = input('Please choose a driver (case insensitive): ').lower()

        if response not in VALID_DRIVERS.keys():
            print('Invalid choice!')
        else:
            return VALID_DRIVERS[response]


def customize_for_driver(driver_name):
    project_directory = os.path.dirname(os.path.abspath(os.path.join(__file__, os.pardir)))

    source = f'{project_directory}/molecule-templates/default-{driver_name}'
    destination = f'{project_directory}/molecule/default'
    shutil.copytree(source, destination)


if __name__ == "__main__":
    driver_name = ask_for_driver()
    customize_for_driver(driver_name)
