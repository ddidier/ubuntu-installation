
# ----------------------------------------------------------------------------------------------------------------------
# Python dependencies required for testing the playbook
# ----------------------------------------------------------------------------------------------------------------------

# --------------------------------------------------------------------------- common dependencies for all playbooks ----

ansible == 2.9.5
docker == 4.2.0
# https://gitlab.com/pycqa/flake8/-/issues/639
flake8 == 3.7.9
molecule == 2.22
python-vagrant == 0.5.15
six == 1.14.0

# ------------------------------------------------------------------------- specific dependencies for this playbook ----

netaddr == 0.7.19
