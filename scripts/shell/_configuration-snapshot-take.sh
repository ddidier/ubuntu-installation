#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd )"

# shellcheck source=/dev/null
source "$SCRIPT_DIR/shflags-v1.2.3/shflags"

# define command-line parameters
DEFINE_string  'directory'  '/var/tmp/snapshots'  'the directory where the snapshots are saved'  'd'
DEFINE_string  'message'    ''                    'the message describing the snapshot'          'm'

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

# define custom help message
# shellcheck disable=SC2034
FLAGS_HELP=$(cat <<'HEREDOC'
Take a snapshot of the user configuration.
Usage: configuration-snapshot take [flags]
HEREDOC
)

function main() {
    local snapshot_directory
    local snapshot_message
    local snapshots_directory

    snapshots_directory="${FLAGS_directory}"
    snapshot_message="${FLAGS_message}"

    if [ ! -d "${snapshots_directory}" ]; then
        echo "The snapshots directory must exist!"
        echo "Check that '${snapshots_directory}' is a directory."
        exit 1
    fi

    snapshot_directory="${snapshots_directory}/$(date +"%Y%m%d-%Hh%Mm%S")"

    if [ -n "${snapshot_message}" ]; then
        snapshot_directory="${snapshot_directory} ${snapshot_message}"
    fi

    echo "Creating snapshot directory '$snapshot_directory'"
    mkdir "${snapshot_directory}"

    # ----------

    local gsettings_file
    gsettings_file="${snapshot_directory}/gsettings.txt"
    echo "Exporting gsettings to '${gsettings_file}'"

    gsettings list-recursively | sort > "${gsettings_file}"

    # ----------

    rsync --archive --recursive \
        --files-from="${SCRIPT_DIR}/_configuration-snapshot-take-rsync-files-from.txt" \
        --exclude-from="${SCRIPT_DIR}/_configuration-snapshot-take-rsync-exclude-from.txt" \
        "${HOME}/" \
        "${snapshot_directory}/"
}

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"
main "$@"

exit 0
