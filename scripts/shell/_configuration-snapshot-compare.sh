#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd )"

# shellcheck source=/dev/null
source "$SCRIPT_DIR/shflags-v1.2.3/shflags"

# define command-line parameters
DEFINE_string  'directory'  '/var/tmp/snapshots'  'the directory where the snapshots are stored'  'd'

set -o pipefail  # trace ERR through pipes
set -o errtrace  # trace ERR through 'time command' and other functions
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

# define custom help message
# shellcheck disable=SC2034
FLAGS_HELP=$(cat <<'HEREDOC'
Compare two snapshots of the user configuration.
Usage: configuration-snapshot compare [flags]
HEREDOC
)

function main() {
    local snapshots_directory

    snapshots_directory="${FLAGS_directory}"

    if [ ! -d "${snapshots_directory}" ]; then
        echo "The snapshots directory must exist!"
        echo "Check that '${snapshots_directory}' is a directory."
        exit 1
    fi

    local last_directories
    local directory_1
    local directory_2
    local directory_name_1
    local directory_name_2

    last_directories="$(find "${snapshots_directory}" -mindepth 1 -maxdepth 1 -printf "%f\n" | sort | tail -n 2)"
    directory_name_1="$(echo "$last_directories" | head -n 1)"
    directory_name_2="$(echo "$last_directories" | tail -n 1)"
    directory_1="${snapshots_directory}/${directory_name_1}"
    directory_2="${snapshots_directory}/${directory_name_2}"

    echo "Comparing '${directory_1}' with '${directory_2}'"

    local diff_installed
    local meld_installed

    set +o errexit
    diff_installed="$(command -v diff)"
    meld_installed="$(command -v meld)"
    set -o errexit

    if [ -n "$meld_installed" ]; then
        meld --newtab "$directory_1" "$directory_2" &
    elif [ -n "$diff_installed" ]; then
        diff -r "$directory_1" "$directory_2"
    else
        echo "No tool to compare snapshots!"
        echo "Please install 'diff' or 'meld'"
        exit 1
    fi
}

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"
main "$@"

exit 0
