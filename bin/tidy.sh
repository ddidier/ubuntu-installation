#!/usr/bin/env bash

# ------------------------------------------------------------------------------
# Tidy source files in order to be compatible with Sphinx:
# - rename image files with incompatible characters
#
# Examples:
#   ./tidy.sh
#   ./tidy.sh --debug
# ------------------------------------------------------------------------------

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/bin/shflags/shflags"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/bin/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/bin/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/bin/ndd-utils4b/ndd-utils4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/bin/variables.sh"



# disable before shflags
ndd::base::catch_more_errors_off

DEFINE_boolean "debug"  false  "Enable debug mode"  "d"

read -r -d '' FLAGS_HELP <<EOF
Tidy source files in order to be compatible with Sphinx:
- rename image files with incompatible characters
Examples:
  ./tidy.sh
EOF

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# enable after shflags
ndd::base::catch_more_errors_on



#
# TODO extract to ndd-utils library
#
# ------------------------------------------------------------------------------
# Join the given array of string to a single string using the given separator.
#
# Arguments:
#   @separator (string) - the separator
#   @strings ([string]) - the array of strings to join
#
# Outputs:
#   Print the joined string to stdout
#
function ndd::strings::join_by() {
    local IFS="${1}"
    shift
    echo "$*"
}



function main() {

    if [[ "${FLAGS_debug}" -eq "${FLAGS_TRUE}" ]]; then
        # shellcheck disable=SC2034
        ndd::logger::set_stdout_level "DEBUG"
    else
        # shellcheck disable=SC2034
        ndd::logger::set_stdout_level "INFO"
    fi

    local source_dir="${PROJECT_DIR}/source"
    local file_extensions=(bmp gif ico jpg jpeg png tiff js html)

    log info "Renaming images"
    log info "- in directory '${source_dir}'"
    log info "- with extensions $(ndd::strings::join_by "," "${file_extensions[@]}")"

    local file_extensions_as_regex
    file_extensions_as_regex=$(ndd::strings::join_by "|" "${file_extensions[@]}")

    readarray -d '' file_paths < <(\
        find "${PROJECT_DIR}/source" \
        -regextype posix-extended -regex "^.+\\.(${file_extensions_as_regex})$" \
        -print0 \
    )

    log info "Found ${#file_paths[@]} image file(s) to check"

    local renamed_files_count=0

    for file_path in "${file_paths[@]}"; do
      log debug "- evaluating ${file_path}"

      local file_name
      local file_directory
      local new_file_name
      local new_file_path

      file_name="$(basename "${file_path}")"
      file_directory="$(dirname "${file_path}")"

      new_file_name=$(            \
          echo "${file_name}"     \
          | sed -r                \
                -e 's/ /_/g'      \
                -e 's/\[//g'      \
                -e 's/\]//g'      \
                -e 's/[àâä]/a/g'  \
                -e 's/[éèêë]/e/g' \
                -e 's/[îï]/i/g'   \
                -e 's/[ôö]/o/g'   \
                -e 's/[ûü]/u/g'   \
      )

      new_file_path="${file_directory}/${new_file_name}"

      if [[ "${new_file_name}" == "${file_name}" ]]; then
        log debug "  - the file name is valid"
    else
        renamed_files_count=$((renamed_files_count + 1))
        log debug "  - the file name is invalid"
        log debug "    renaming '${file_path}'"
        log debug "          to '${new_file_path}'"
        mv "${file_path}" "${new_file_path}"
      fi

    done

    log info "Renamed ${renamed_files_count} image file(s)"
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    log error "An unexpected error has occured:\n%s" "$(ndd::base::print_stack_trace 2>&1)"

    exit 1
}

trap 'error_handler ${?}' ERR

main "${@}"

exit 0
